# Clojurescript SPA project template

This project template leverages clj-new to generate clojurescript single page applications with either reagent, reframe or rum
in addition to figwheel and stylefy

## Setup

add this alias to your `~/.clojure/deps.edn` file

```
  :new-spa
  {:extra-deps {seancorfield/clj-new
                {:mvn/version "0.5.5"}}
   :main-opts ["-m" "clj-new.create" "https://gitlab.com/pbaille/cljs-spa@1ef3fee371c6d5a54913edbad04c43c397fa5ee7"]}}
```

## Usage

### reagent

`clj -A:new-spa me/proj1`

### re-frame

`clj -A:new-spa me/proj1 +re-frame`

### rum

`clj -A:new-spa me/proj +rum`

## License

why?



(ns {{class}}.core
  (:require
    [reagent.core :as r]
    [re-frame.core :as rf]
    [{{class}} .styles :as ss]
    [stylefy.core :as stylefy
     :refer [use-style sub-style use-sub-style]]))

(def initial-db {})

(rf/reg-event-db
  :init
  (fn [_] initial-db))

(rf/reg-sub
  :at
  (fn [db [_ & segs]]
      (get-in db segs)))

(defn root-comp []
   (let [greeting (rf/subscribe [:at :greeting])]
      [:h1
       (use-style ss/header)
       @greeting]))

(defn ^:export init []
      (rf/dispatch [:init])
      (ss/init)
      (r/render [root-comp]
                (js/document.getElementById "app")))

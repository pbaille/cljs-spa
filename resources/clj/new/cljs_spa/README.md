# {{name}}

TODO (project description)

## Cursive setup

This is the minimal steps to setup a project 

`clj -A:new path/to/template/repo domain/project`

open the generated project with cursive

a window should pop, check `Use auto-import`

wait a bit for intellij to sync

go to `Run` then `edit configurations`

check `clojure.main` and `Run with Deps`
feel the `Aliases` field with `fig`

apply changes and close

launch the repl, enjoy! :)

## Compilation

`clj -A:build`


(ns {{class}}.core
  (:require
    [rum.core :as rum]
    [{{class}} .styles :as ss]
    [stylefy.core :as stylefy
     :refer [use-style sub-style use-sub-style]]))

(def count (atom 0))

(rum/defc counter < rum/reactive []
   [:div
    {:on-click (fn [_] (swap! count inc))}
     "Clicks: " (rum/react count)])

(defn ^:export init []
   (ss/init)
   (rum/mount (counter)
              (js/document.getElementById "app")))
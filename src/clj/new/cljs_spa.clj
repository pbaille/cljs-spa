(ns clj.new.cljs-spa
  (:require [clj.new.templates :as ts :refer [renderer sanitize-ns project-name name-to-path ->files]]
            [clojure.java.io :as io]))

(def render (renderer "cljs-spa"))

(defn cljs-spa
  [name & opts]
  (let [opts (set opts)
        data {:name (project-name name)
              :class (sanitize-ns name)
              :path (name-to-path name)
              :re-frame (opts "+re-frame")
              :rum (opts "+rum")}]

    (println "Generating cljs-spa fresh project " data)

    (->files data
             ["README.md" (render "README.md" data)]
             [".gitignore" (render ".gitignore" data)]
             ["dev.cljs.edn" (render "dev.cljs.edn" data)]
             ["resources/public/index.html" (render "index.html" data)]
             ["src/{{path}}/core.cljs"
              (cond
                (:re-frame data) (render "re_frame/core.cljs" data)
                (:rum data) (render "rum/core.cljs" data)
                :else (render "reagent/core.cljs" data))]
             ["deps.edn"
              (cond
                (:re-frame data) (render "re_frame/deps.edn" data)
                (:rum data) (render "rum/deps.edn" data)
                :else (render "reagent/deps.edn" data))]
             ["src/{{path}}/styles.cljs" (render "styles.cljs" data)])))


